package org.shana.springframework.context;

import org.shana.springframework.beans.BeansException;
import org.shana.springframework.beans.factory.Aware;
import org.shana.springframework.context.ApplicationContext;

/**
 * Interface to be implemented by any object that wishes to be notifiedof the {@link ApplicationContext} that it runs in.
 * 实现此接口，既能感知到所属的 ApplicationContext
 */
public interface ApplicationContextAware extends Aware {

    void setApplicationContext(ApplicationContext applicationContext) throws BeansException;

}
