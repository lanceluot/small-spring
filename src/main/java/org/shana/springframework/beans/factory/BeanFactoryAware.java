package org.shana.springframework.beans.factory;

import org.shana.springframework.beans.BeansException;

public interface BeanFactoryAware extends Aware {

   void setBeanFactory(BeanFactory beanFactory) throws BeansException;

}
