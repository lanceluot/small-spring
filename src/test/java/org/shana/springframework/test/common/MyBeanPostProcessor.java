//package org.shana.springframework.test.common;
//
//import org.shana.springframework.beans.BeansException;
//import org.shana.springframework.beans.factory.config.BeanPostProcessor;
//import org.shana.springframework.test.bean.UserService;
//
//public class MyBeanPostProcessor implements BeanPostProcessor {
//
//    @Override
//    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
//        if ("userService".equals(beanName)) {
//            UserService userService = (UserService) bean;
//            userService.setLocation("北京");
//        }
//        return bean;
//    }
//
//    @Override
//    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
//        return bean;
//    }
//
//}
