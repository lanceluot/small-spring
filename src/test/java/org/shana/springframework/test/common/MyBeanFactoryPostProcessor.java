package org.shana.springframework.test.common;

import org.shana.springframework.beans.BeansException;
import org.shana.springframework.beans.PropertyValue;
import org.shana.springframework.beans.PropertyValues;
import org.shana.springframework.beans.factory.ConfigurableListableBeanFactory;
import org.shana.springframework.beans.factory.config.BeanDefinition;
import org.shana.springframework.beans.factory.config.BeanFactoryPostProcessor;

public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("userService");
        PropertyValues propertyValues = beanDefinition.getPropertyValues();

        propertyValues.addPropertyValue(new PropertyValue("company", "spacex"));
    }

}
