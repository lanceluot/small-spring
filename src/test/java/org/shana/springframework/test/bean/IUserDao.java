package org.shana.springframework.test.bean;

public interface IUserDao {

    String queryUserName(String uId);

}
