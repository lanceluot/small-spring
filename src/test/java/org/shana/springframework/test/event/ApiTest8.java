package org.shana.springframework.test.event;

import org.junit.Test;
import org.shana.springframework.context.support.ClassPathXmlApplicationContext;

public class ApiTest8 {
    @Test
    public void test_event() {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:spring.xml");
        applicationContext.publishEvent(new CustomEvent(applicationContext, 1019129009086763L, "成功了！"));

        applicationContext.registerShutdownHook();
    }
}
