package org.shana.springframework.test.explore;

import java.lang.reflect.Method;

public class ReflectionExample {
    public static void main(String[] args) throws Exception {
        // 创建 UserService 实例
        UserService userService = new UserService();

        userService.id="117";
        
        // 获取 UserService 类的 greet 方法
        Method method = UserService.class.getMethod("greet", String.class);
        
        // 调用 greet 方法，传入实例对象和参数
        method.invoke(userService, "Alice");
    }
}
